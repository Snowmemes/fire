#include <iostream>
#include <deque>
#include <set>

using namespace std;

enum TileKind {
    PASSABLE = -1, WALL = -2, FIRE = -3
};

struct Pos {
    int x, y;
};

void load_input(int w, int h, int*** maze, deque<Pos>* fire, Pos &start_pos) {
    for (int y = 1; y < h + 1; y++) {
        for (int x = 1; x < w + 1; x++) {
            char symbol;
            cin >> symbol;

            if (symbol == '.') {
                (*maze)[y][x] = PASSABLE;
            } else if (symbol == 'J') {
                (*maze)[y][x] = PASSABLE;
                start_pos.x = x;
                start_pos.y = y;
            } else if (symbol == 'F') {
                (*maze)[y][x] = FIRE;
                Pos fire_pos = Pos();
                fire_pos.x = x;
                fire_pos.y = y;

                fire->push_back(fire_pos);
            } else if (symbol == '#') {
                (*maze)[y][x] = WALL;
            }
        }
    }
}

void bfs(int*** maze, bool*** visited, deque<Pos>* f_q, Pos p) {
    deque<Pos> player_queue = deque<Pos>();
    deque<Pos> fire_queue = *f_q;
    player_queue.push_back(p);
    bool** v = *visited;
    int** m = *maze;

    m[p.y][p.x] = 1;
    int steps;

    int dir_x[] = {0,  0,  1, -1 };
    int dir_y[] = {1, -1,  0,  0 };

    bool success = false;

	while (!success) {
		deque<Pos> next_fire_queue = deque<Pos>();

		while (!fire_queue.empty()) {
			p = fire_queue.front();
			fire_queue.pop_front();

			for (int i = 0; i < 4; i++) {
				int nbr = m[p.y + dir_y[i]][p.x + dir_x[i]];

				if (nbr == PASSABLE) {
					Pos n_pos = Pos();
					n_pos.x = p.x + dir_x[i];
					n_pos.y = p.y + dir_y[i];

					m[p.y + dir_y[i]][p.x + dir_x[i]] = FIRE;

					if (!v[n_pos.y][n_pos.x]) {
						next_fire_queue.push_back(n_pos);
					}
				}
			}
		}

		fire_queue = next_fire_queue;

		deque<Pos> next_player_queue = deque<Pos>();
		while (!player_queue.empty()) {
			p = player_queue.front();
			player_queue.pop_front();

			v[p.y][p.x] = true;

			for (int i = 0; i < 4; i++) {
				int nbr = m[p.y + dir_y[i]][p.x + dir_x[i]];

				if (nbr == PASSABLE) {
					Pos n_pos = Pos();
					n_pos.x = p.x + dir_x[i];
					n_pos.y = p.y + dir_y[i];

					if (!v[n_pos.y][n_pos.x]) {
						m[n_pos.y][n_pos.x] = m[p.y][p.x] + 1;
						next_player_queue.push_back(n_pos);
					}
				} else if (nbr == 0) {
					steps = m[p.y][p.x];

					success = true;
					break;
				}
			}
		}

		player_queue = next_player_queue;

		if (player_queue.empty()) {
			break;
		}
	}

    if (!success) {
        cout << "IMPOSSIBLE" << endl;
    } else {
        cout << steps << endl;
    }
}

void print_maze(int*** maze, int w, int h) {
    for (int y = 1; y < h + 1; y++) {
        for (int x = 1; x < w + 1; x++) {
            int value = (*maze)[y][x];
            char symbol;

            switch (value) {
                case WALL:
                    symbol = '#';
                    break;
                case FIRE:
                    symbol = 'F';
                    break;
                case PASSABLE:
                    symbol = '.';
                    break;
                default:
                    symbol = '?';
                    break;
            }

            cout << symbol;
        }

        cout << endl;
    }

	cout << endl;
}

int main() {
    int n;
    cin >> n;

    for (int i = 0; i < n; i++) {
        int w, h;
        cin >> h >> w;
        Pos start_pos = Pos();

        int** maze = new int*[h + 2];
        bool** visited = new bool*[h + 2];
        for (int y = 0; y < h + 2; y++) {
            maze[y] = new int[w + 2];
            visited[y] = new bool[w + 2];

            for (int x = 0; x < w + 2; x++) {
                maze[y][x] = 0;
                visited[y][x] = false;
            }
        }

        deque<Pos> fire = deque<Pos>();
        load_input(w, h, &maze, &fire, start_pos);
        //print_maze(&maze, w, h);

        bfs(&maze, &visited, &fire, start_pos);
        //print_maze(&maze, w, h);
    }

    return 0;
}
