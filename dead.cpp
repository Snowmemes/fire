#include <iostream>
#include <vector>

using namespace std;

enum NodeType {
    PASSABLE = 0, ON_FIRE = 3, WALL = 5, EXIT = 1
};

enum Direction {
    LEFT = 0, RIGHT = 1, UP = 2, DOWN = 3
};

struct Node {
    Node** neighbours;
    NodeType type;
    int x;
    int y;
};

int reverse(int direction) {
    switch (direction) {
        case LEFT:
            return RIGHT;
        case RIGHT:
            return LEFT;
        case UP:
            return DOWN;
        case DOWN:
            return UP;
        default:
            return -1;
    }
}

void load_input(int )

void load_input(Node* origin, Node** start_pos) {
    origin->neighbours = new Node*[4];

    int w, h;

    cin >> h;
    cin >> w;

    Node* node = origin;
    Node* row_start = new Node();
    row_start->neighbours = new Node*[4];
    row_start->neighbours[DOWN] = node;
    Node* above_origin = row_start;
    Node* node_above = nullptr;
    Node* previous = nullptr;
    int direction = LEFT;

    for (int y = 0; y < h; y++) {
        cout << "  NEXT ROW" << endl;
        node_above = y == 0 ? nullptr : row_start;
        row_start = node;

        if (previous != nullptr) {
            previous->neighbours[RIGHT] = nullptr;
            node->neighbours[LEFT] = nullptr;
        }

        for (int x = 0; x < w; x++) {
            char tile;
            cin >> tile;

            cout << "got " << tile << endl;

            previous = node;
            node->neighbours[direction] = new Node();
            node->x = x;
            node->y = y;
            node = node->neighbours[direction];

            node->neighbours = new Node*[4];
            node->neighbours[LEFT] = nullptr;
            node->neighbours[RIGHT] = nullptr;
            node->neighbours[UP] = node_above;
            node->neighbours[DOWN] = nullptr;
            node->neighbours[reverse(direction)] = previous;

            NodeType type;
            switch (tile) {
                case '#':
                    type = WALL;
                    break;
                case '.':
                    type = PASSABLE;
                    break;
                case 'J':
                    type = PASSABLE;
                    *start_pos = node;
                    break;
                case 'F':
                    type = ON_FIRE;
                    break;
            }

            node->type = type;

            if (node_above != nullptr) {
                node_above->neighbours[DOWN] = node;
                node_above = node_above->neighbours[RIGHT];
            }

            direction = RIGHT;
        }

        direction = DOWN;
    }

    origin->neighbours[UP] = nullptr;
    delete[] above_origin->neighbours;
    delete above_origin;
}

void bfs(Node& top_left) {
    vector<Node> open = vector<Node>();
}

void render_maze(Node* top_left) {
    Node *next_row = top_left->neighbours[DOWN];

    while (top_left != nullptr) {
        while (top_left != nullptr) {
            cout << top_left->type;
            top_left = top_left->neighbours[RIGHT];
        }

        cout << endl;

        top_left = next_row;
        next_row = next_row->neighbours[DOWN];
    }
}

int main() {
    int n = 0;
    cin >> n;

    for (int i = 0; i < n; i++) {
        Node top_left = Node();
        Node *start_pos;
        load_input(&top_left, &start_pos);
        render_maze(&top_left);
    }

    std::cout << "glhf" << std::endl;
    return 0;
}